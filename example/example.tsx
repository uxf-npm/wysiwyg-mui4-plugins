import { createTheme, ThemeProvider } from "@material-ui/core/styles";
import React from "react";
import { render } from "react-dom";
import { App } from "./App";

render(
    <ThemeProvider theme={createTheme()}>
        <App />
    </ThemeProvider>,
    document.getElementById("app"),
);
