import { Box } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent/CardContent";
import CardHeader from "@material-ui/core/CardHeader/CardHeader";
import { EditorImageElementPlugin, WysiwygContent, WysiwygEditor } from "@uxf/wysiwyg";
import { RenderComponent, ContentRenderer } from "@uxf/wysiwyg/Content";
import React, { FC } from "react";
import { useField } from "react-final-form";
import {
    BlockQuotePlugin,
    BoldPlugin,
    ButtonPlugin,
    CodePlugin,
    Divider,
    HeadingPlugins,
    ImagePlugin,
    ItalicPlugin,
    LinkPlugin,
    ListBulletedPlugin,
    ListItemPlugin,
    ListNumberedPlugin,
    ParagraphPlugin,
    Toolbar,
    UnderlinePlugin,
    VideoPlugin,
} from "../src";
import { ButtonElement, ParagraphComponent } from "./components";

// image url mock
const imageUrlClient = () => "https://cdn.pixabay.com/photo/2020/05/20/06/47/mountain-5195052_960_720.jpg";

const imageUploadHandler = () => {
    return Promise.resolve({
        id: 1,
        uuid: "uuid",
        type: "image",
        extension: ".jpg",
        name: "name",
        namespace: "blog",
        url: "https://cdn.pixabay.com/photo/2020/05/20/06/47/mountain-5195052_960_720.jpg",
    });
};

export const WysiwygField: FC = () => {
    const { input } = useField<WysiwygContent>("wysiwyg");

    const MyImagePlugin: EditorImageElementPlugin = {
        ...ImagePlugin,
        imageUrlClient: imageUrlClient,
        imageUploadHandler: imageUploadHandler,
        imageNamespace: "blog",
    };

    return (
        <>
            <Box mb={2}>
                <WysiwygEditor
                    onChange={input.onChange}
                    value={input.value}
                    Toolbar={Toolbar}
                    plugins={[
                        ParagraphPlugin,
                        ...HeadingPlugins,
                        Divider,
                        ItalicPlugin,
                        BoldPlugin,
                        UnderlinePlugin,
                        CodePlugin,
                        BlockQuotePlugin,
                        Divider,
                        LinkPlugin,
                        ButtonPlugin,
                        Divider,
                        ListNumberedPlugin,
                        ListBulletedPlugin,
                        ListItemPlugin,
                        Divider,
                        VideoPlugin,
                        MyImagePlugin,
                    ]}
                />
            </Box>
            <Box>
                <Card style={{ minHeight: 200 }}>
                    <CardHeader title="Result:" />
                    <CardContent>
                        <ContentRenderer<{ prop: string }, { "solar-lead": RenderComponent }>
                            data={input.value}
                            components={{
                                buttonComponent: ButtonElement,
                                customRenderers: {
                                    "solar-lead": ParagraphComponent,
                                },
                            }}
                            defaultComponentProps={{
                                button: {
                                    ownProps: {
                                        prop: "This is custom prop passed to button component",
                                    },
                                },
                            }}
                        />
                    </CardContent>
                </Card>
            </Box>
        </>
    );
};

WysiwygField.displayName = "WysiwygField";
