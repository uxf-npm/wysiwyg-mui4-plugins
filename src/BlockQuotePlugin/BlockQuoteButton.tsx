import FormatQuoteIcon from "@material-ui/icons/FormatQuote";
import React, { FC } from "react";
import { ToolbarButtonProps } from "@uxf/wysiwyg";
import { ElementTypeButton } from "../ElementTypeButton";

export const BlockQuoteButton: FC<ToolbarButtonProps> = ({ editor }) => (
    <ElementTypeButton editor={editor} label={<FormatQuoteIcon />} name="block-quote" title="Citace" />
);
