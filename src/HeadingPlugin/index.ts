import { H1Plugin, H2Plugin, H3Plugin, H4Plugin, H5Plugin, H6Plugin } from "./HeadingPlugin";

export { H1Plugin, H2Plugin, H3Plugin, H4Plugin, H5Plugin, H6Plugin } from "./HeadingPlugin";
export const HeadingPlugins = [H1Plugin, H2Plugin, H3Plugin, H4Plugin, H5Plugin, H6Plugin];
