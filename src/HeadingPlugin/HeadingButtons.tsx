import React, { FC } from "react";
import { ToolbarButtonProps } from "@uxf/wysiwyg";
import { ElementTypeButton } from "../ElementTypeButton";

export const H1Button: FC<ToolbarButtonProps> = ({ editor }) => (
    <ElementTypeButton editor={editor} label={<>H1</>} name="h1" title="Nadpis H1" />
);

export const H2Button: FC<ToolbarButtonProps> = ({ editor }) => (
    <ElementTypeButton editor={editor} label={<>H2</>} name="h2" title="Nadpis H2" />
);

export const H3Button: FC<ToolbarButtonProps> = ({ editor }) => (
    <ElementTypeButton editor={editor} label={<>H3</>} name="h3" title="Nadpis H3" />
);

export const H4Button: FC<ToolbarButtonProps> = ({ editor }) => (
    <ElementTypeButton editor={editor} label={<>H4</>} name="h4" title="Nadpis H4" />
);

export const H5Button: FC<ToolbarButtonProps> = ({ editor }) => (
    <ElementTypeButton editor={editor} label={<>H5</>} name="h5" title="Nadpis H5" />
);

export const H6Button: FC<ToolbarButtonProps> = ({ editor }) => (
    <ElementTypeButton editor={editor} label={<>H6</>} name="h6" title="Nadpis H6" />
);
