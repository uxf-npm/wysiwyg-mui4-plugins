import { Box } from "@material-ui/core";
import React, { FC } from "react";
import { useFocused, useSelected } from "slate-react";
import { EditorImageElementRendererProps } from "@uxf/wysiwyg";
import { useImageStyles } from "./styles";

export const ImageEditorElement: FC<EditorImageElementRendererProps> = ({
    attributes,
    element,
    imageUrlClient,
    imageNamespace,
    children,
}) => {
    const selected = useSelected();
    const focused = useFocused();
    const classes = useImageStyles({ selected, focused });

    return (
        <Box {...attributes} mb={2}>
            <figure contentEditable={false}>
                <img
                    src={imageUrlClient(element.uuid, element.extension, imageNamespace)}
                    className={classes.image}
                    alt={element.alt}
                />
                {(element.caption || element.source) && (
                    <figcaption>
                        {element.caption}
                        {element.source && (
                            <>
                                {element.caption && <br />}
                                <cite>{element.source}</cite>
                            </>
                        )}
                    </figcaption>
                )}
            </figure>
            {children}
        </Box>
    );
};
