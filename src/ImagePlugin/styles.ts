import { Theme } from "@material-ui/core/styles/createTheme";
import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

interface ImageStyleProps {
    focused: boolean;
    selected: boolean;
}

export const useImageStyles = makeStyles<Theme, ImageStyleProps, "image">(theme =>
    createStyles({
        image: {
            display: "block",
            maxWidth: "100%",
            boxShadow: ({ selected, focused }) =>
                selected && focused ? `0 0 0 3px ${theme.palette.action.active}` : "none",
        },
    }),
);
