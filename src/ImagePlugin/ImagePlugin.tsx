import { EditorImageElementPlugin } from "@uxf/wysiwyg";
import { ImageEditorElement } from "./ImageEditorElement";
import { ImageElementButton } from "./ImageElementButton";

export const ImagePlugin: EditorImageElementPlugin = {
    toolbarButton: ImageElementButton,
    editorElementRenderer: ImageEditorElement,
    imageUrlClient: () => {
        // eslint-disable-next-line no-console
        console.error("Implement client!");
        return "CLIENT NOT IMPLEMENTED";
    },

    imageUploadHandler: () => {
        // eslint-disable-next-line no-console
        console.error("Implement upload handler!");
        return Promise.resolve({
            id: 0,
            type: "Implement upload handler!",
            name: "Implement upload handler!",
            url: "Implement upload handler!",
            extension: "Implement upload handler!",
            uuid: "Implement upload handler!",
            namespace: "Implement upload handler!",
        });
    },
    imageNamespace: "NAMESPACE NOT SET",
    type: "image",
};
