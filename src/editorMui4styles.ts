import createStyles from "@material-ui/core/styles/createStyles";
import { Theme } from "@material-ui/core/styles/createTheme";
import makeStyles from "@material-ui/core/styles/makeStyles";

export const editorMui4styles = makeStyles((theme: Theme) =>
    createStyles({
        editable: {
            border: `1px solid ${theme.palette.divider}`,
            borderRadius: theme.spacing(0, 0, 0.5, 0.5),
            marginTop: -11,
            marginLeft: -4,
            marginRight: -4,
            padding: theme.spacing(2),
            minHeight: theme.spacing(8),
        },
    }),
);
