import React, { FC } from "react";
import { EditorLeafRendererProps } from "@uxf/wysiwyg";

export const ItalicEditorRenderer: FC<EditorLeafRendererProps> = ({ children, leaf }) => {
    if (leaf.italic) {
        return <em>{children}</em>;
    }

    return children;
};
