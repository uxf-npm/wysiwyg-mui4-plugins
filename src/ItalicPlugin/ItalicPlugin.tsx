import { EditorLeafPlugin } from "@uxf/wysiwyg";
import { ItalicButton } from "./ItalicButton";
import { ItalicEditorRenderer } from "./ItalicEditorRenderer";

export const ItalicPlugin: EditorLeafPlugin = {
    toolbarButton: ItalicButton,
    editorLeafRenderer: ItalicEditorRenderer,
    type: "italic",
};
