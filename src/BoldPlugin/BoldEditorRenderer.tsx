import React, { FC } from "react";
import { EditorLeafRendererProps } from "@uxf/wysiwyg";

export const BoldEditorRenderer: FC<EditorLeafRendererProps> = ({ children, leaf }) => {
    if (leaf.bold) {
        return <strong>{children}</strong>;
    }

    return children;
};
