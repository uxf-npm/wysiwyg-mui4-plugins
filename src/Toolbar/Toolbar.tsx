import Paper from "@material-ui/core/Paper";
import { ToolbarComponent } from "@uxf/wysiwyg";
import React from "react";
import { useSlate } from "slate-react";
import { toolbarStyles } from "./styles";

export const Toolbar: ToolbarComponent = ({ plugins }) => {
    const classes = toolbarStyles();
    const editor = useSlate();
    return (
        <div className={classes.grouped}>
            <Paper elevation={0} className={classes.paper}>
                {plugins.map((Plugin, index) => {
                    if ("buttonDivider" in Plugin) {
                        return <Plugin.buttonDivider key={`divider-${index}`} />;
                    }

                    if (typeof Plugin.toolbarButton === "undefined") {
                        return null;
                    }

                    if ("imageUploadHandler" in Plugin && "imageNamespace" in Plugin) {
                        return (
                            <Plugin.toolbarButton
                                key={Plugin.type + index}
                                editor={editor}
                                imageNamespace={Plugin.imageNamespace}
                                imageUploadHandler={Plugin.imageUploadHandler}
                            />
                        );
                    }

                    return <Plugin.toolbarButton key={Plugin.type + index} editor={editor} />;
                })}
            </Paper>
        </div>
    );
};
