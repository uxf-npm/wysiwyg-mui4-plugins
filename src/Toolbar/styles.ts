import createStyles from "@material-ui/core/styles/createStyles";
import { Theme } from "@material-ui/core/styles/createTheme";
import makeStyles from "@material-ui/core/styles/makeStyles";

export const toolbarStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            display: "flex",
            alignItems: "center",
            border: `1px solid ${theme.palette.divider}`,
            borderRadius: theme.spacing(0.5, 0.5, 0, 0),
            flexWrap: "wrap",
            margin: theme.spacing(1, -2),
        },
        grouped: {
            margin: theme.spacing(0.5),
            border: "none !important",
            padding: theme.spacing(0, 1),
            "&:not(:first-child)": {
                borderRadius: theme.shape.borderRadius,
            },
            "&:first-child": {
                borderRadius: theme.shape.borderRadius,
            },
            position: "sticky",
            top: 0,
            zIndex: 1000,
        },
    }),
);
