import MuiDivider from "@material-ui/core/Divider";
import React, { FC } from "react";
import { ToolbarButtonDivider } from "@uxf/wysiwyg";
import { dividerStyles } from "./styles";

const DividerComponent: FC = () => {
    const classes = dividerStyles();
    return <MuiDivider orientation="vertical" className={classes.divider} />;
};

export const Divider: ToolbarButtonDivider = {
    buttonDivider: DividerComponent,
};
