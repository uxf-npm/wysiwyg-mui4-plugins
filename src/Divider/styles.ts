import createStyles from "@material-ui/core/styles/createStyles";
import { Theme } from "@material-ui/core/styles/createTheme";
import makeStyles from "@material-ui/core/styles/makeStyles";

export const dividerStyles = makeStyles((theme: Theme) =>
    createStyles({
        divider: {
            alignSelf: "stretch",
            height: "auto",
            margin: theme.spacing(1, 0.5),
        },
    }),
);
