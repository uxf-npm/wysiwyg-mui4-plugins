import { EditorElementPlugin } from "@uxf/wysiwyg";
import { LinkButton } from "./LinkButton";
import { LinkEditorElement } from "./LinkEditorElement";

export const LinkPlugin: EditorElementPlugin = {
    toolbarButton: LinkButton,
    editorElementRenderer: LinkEditorElement,
    type: "link",
};
