import React, { FC } from "react";
import { EditorElementRendererProps } from "@uxf/wysiwyg";

export const LinkEditorElement: FC<EditorElementRendererProps> = ({ attributes, element, children }) => (
    <a
        {...attributes}
        href={typeof element.url === "string" ? element.url : undefined}
        target={typeof element.target === "string" ? element.target : undefined}
    >
        {children}
    </a>
);
