import FormatUnderlinedIcon from "@material-ui/icons/FormatUnderlined";
import React, { FC } from "react";
import { ToolbarButtonProps } from "@uxf/wysiwyg";
import { LeafTypeButton } from "../LeafTypeButton";

export const UnderlineButton: FC<ToolbarButtonProps> = ({ editor }) => (
    <LeafTypeButton editor={editor} label={<FormatUnderlinedIcon />} name="underline" title="Podtržení" />
);
