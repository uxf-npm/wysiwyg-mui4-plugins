import CodeIcon from "@material-ui/icons/Code";
import React, { FC } from "react";
import { ToolbarButtonProps } from "@uxf/wysiwyg";
import { LeafTypeButton } from "../LeafTypeButton";

export const CodeButton: FC<ToolbarButtonProps> = ({ editor }) => (
    <LeafTypeButton editor={editor} label={<CodeIcon />} name="code" title="Zdrojový kód" />
);
