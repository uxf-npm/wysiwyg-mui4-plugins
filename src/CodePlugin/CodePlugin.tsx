import { EditorLeafPlugin } from "@uxf/wysiwyg";
import { CodeButton } from "./CodeButton";
import { CodeEditorRenderer } from "./CodeEditorRenderer";

export const CodePlugin: EditorLeafPlugin = {
    toolbarButton: CodeButton,
    editorLeafRenderer: CodeEditorRenderer,
    type: "code",
};
