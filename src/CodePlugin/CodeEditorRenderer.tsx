import React, { FC } from "react";
import { EditorLeafRendererProps } from "@uxf/wysiwyg";

export const CodeEditorRenderer: FC<EditorLeafRendererProps> = ({ children, leaf }) => {
    if (leaf.code) {
        return <code>{children}</code>;
    }

    return children;
};
