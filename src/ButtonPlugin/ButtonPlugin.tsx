import { EditorElementPlugin } from "@uxf/wysiwyg";
import { ButtonEditorElement } from "./ButtonEditorElement";
import { ButtonElementButton } from "./ButtonElementButton";

export const ButtonPlugin: EditorElementPlugin = {
    toolbarButton: ButtonElementButton,
    editorElementRenderer: ButtonEditorElement,
    type: "button",
};
