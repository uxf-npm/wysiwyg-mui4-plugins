import ToggleButton from "@material-ui/lab/ToggleButton";
import React, { FC } from "react";
import { toggleButtonStyles } from "../toggleButtonStyles";
import { ElementTypeButtonProps } from "./types";

export const ElementTypeButton: FC<ElementTypeButtonProps> = ({ editor, label, name, title }) => {
    const classes = toggleButtonStyles();
    return (
        <ToggleButton
            key={name}
            className={classes.button}
            size="small"
            title={title}
            value={name}
            aria-label={name}
            selected={editor.isElementActive(editor, name)}
            onMouseDown={event => {
                editor.toggleElement(editor, name);
                event.preventDefault();
            }}
        >
            {label}
        </ToggleButton>
    );
};
