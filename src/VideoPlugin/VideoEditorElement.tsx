import React from "react";
import ReactPlayer from "react-player/lazy";
import { RenderElementProps, useFocused, useSelected } from "slate-react";
import { useVideoStyles } from "./styles";

export const VideoEditorElement = ({ attributes, element }: Omit<RenderElementProps, "children">) => {
    const selected = useSelected();
    const focused = useFocused();
    const classes = useVideoStyles({ selected, focused });
    return (
        <div {...attributes} className={classes.wrapper}>
            <ReactPlayer
                className={classes.player}
                url={typeof element.url === "string" ? element.url : ""}
                controls={true}
                width="100%"
                height="100%"
            />
        </div>
    );
};
