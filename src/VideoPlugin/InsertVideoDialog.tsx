import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import React, { FC, useCallback, useMemo, useState } from "react";
import ReactPlayer from "react-player/lazy";
import { EditorVideoElement, UXFEditor } from "@uxf/wysiwyg";

export interface InsertVideoDialogProps {
    open: boolean;
    onClose: () => void;
    editor: UXFEditor;
    activeVideo: EditorVideoElement | null;
}

export const InsertVideoDialog: FC<InsertVideoDialogProps> = ({ open, onClose, editor, activeVideo }) => {
    const [url, setUrl] = useState<string>(activeVideo?.url ?? "");

    const resetState = useCallback(() => {
        setUrl("");
    }, []);

    // Cancel button
    const closeHandler = useCallback(() => {
        resetState();
        onClose();
    }, [onClose, resetState]);

    // OK button
    const okButtonHandler = () => {
        editor.insertVideo(editor, url, editor.selection);
        resetState();
        onClose();
    };

    const playableVideo = useMemo(() => url && ReactPlayer.canPlay(url), [url]);

    return (
        <Dialog open={open} onClose={closeHandler} aria-labelledby="insert-video-dialog" fullWidth>
            <DialogTitle id="insert-video-dialog-title">Vložit video</DialogTitle>
            <DialogContent>
                <TextField
                    margin="dense"
                    id="url"
                    label="URL videa"
                    type="text"
                    fullWidth
                    value={url}
                    required
                    onChange={e => setUrl(e.target.value)}
                />
                {playableVideo && <ReactPlayer url={url} width="100%" controls={true} />}
            </DialogContent>

            <DialogActions>
                <Button onClick={closeHandler} color="secondary">
                    Zrušit
                </Button>
                <Button onClick={okButtonHandler} color="primary" variant="contained" disabled={!playableVideo}>
                    Vložit video
                </Button>
            </DialogActions>
        </Dialog>
    );
};
