import VideoIcon from "@material-ui/icons/OndemandVideo";
import ToggleButton from "@material-ui/lab/ToggleButton";
import React, { FC, useCallback, useState } from "react";
import { EditorVideoElement, ToolbarButtonProps } from "@uxf/wysiwyg";
import { toggleButtonStyles } from "../toggleButtonStyles";
import { InsertVideoDialog } from "./InsertVideoDialog";

export const VideoElementButton: FC<ToolbarButtonProps> = ({ editor }) => {
    const classes = toggleButtonStyles();
    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [activeVideo, setActiveVideo] = useState<EditorVideoElement | null>(null);

    const closeDialog = useCallback(() => setDialogOpen(false), []);

    return (
        <>
            <ToggleButton
                key="video-button"
                className={classes.button}
                size="small"
                value="video"
                aria-label="video"
                selected={editor.isVideoActive(editor)}
                title="Video"
                onClick={e => {
                    e.preventDefault();
                    e.persist();
                    setActiveVideo(editor.getActiveVideo(editor));
                    setDialogOpen(true);
                }}
            >
                <VideoIcon />
            </ToggleButton>
            <InsertVideoDialog open={dialogOpen} onClose={closeDialog} editor={editor} activeVideo={activeVideo} />
        </>
    );
};
