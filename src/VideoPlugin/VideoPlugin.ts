import { EditorElementPlugin } from "@uxf/wysiwyg";
import { VideoEditorElement } from "./VideoEditorElement";
import { VideoElementButton } from "./VideoElementButton";

export const VideoPlugin: EditorElementPlugin = {
    toolbarButton: VideoElementButton,
    editorElementRenderer: VideoEditorElement,
    type: "video",
};
