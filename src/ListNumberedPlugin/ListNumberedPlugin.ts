import { EditorElementPlugin } from "@uxf/wysiwyg";
import { ListNumberedButton } from "./ListNumberedButton";
import { ListNumberedEditorElement } from "./ListNumberedEditorElement";

export const ListNumberedPlugin: EditorElementPlugin = {
    toolbarButton: ListNumberedButton,
    editorElementRenderer: ListNumberedEditorElement,
    type: "numbered-list",
};
