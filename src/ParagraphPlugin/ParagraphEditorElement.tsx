import Typography from "@material-ui/core/Typography/Typography";
import React, { FC } from "react";
import { EditorElementRendererProps } from "@uxf/wysiwyg";

export const ParagraphEditorElement: FC<EditorElementRendererProps> = ({ attributes, children }) => (
    <Typography variant="body1" {...attributes} style={{ marginBottom: "1em" }}>
        {children}
    </Typography>
);
