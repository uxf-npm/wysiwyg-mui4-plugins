import ToggleButton from "@material-ui/lab/ToggleButton";
import React, { FC } from "react";
import { toggleButtonStyles } from "../toggleButtonStyles";
import { LeafTypeButtonProps } from "./types";

export const LeafTypeButton: FC<LeafTypeButtonProps> = ({ editor, label, name, title }) => {
    const classes = toggleButtonStyles();
    return (
        <ToggleButton
            key={name}
            className={classes.button}
            size="small"
            title={title}
            value={name}
            aria-label={name}
            selected={editor.isMarkActive(editor, name)}
            onMouseDown={event => {
                editor.toggleMark(editor, name);
                event.preventDefault();
            }}
        >
            {label}
        </ToggleButton>
    );
};
